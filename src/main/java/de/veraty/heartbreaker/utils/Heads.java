package de.veraty.heartbreaker.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.SkullMeta;

public class Heads {

    public static final ItemStack MINION = getHead("§r§lMinion", "RealMinion"), PIRATE = getHead("§r§lPirat", "samsamsam1234"),
            DWARF = getHead("§r§lZwerg", "platypus99"), KNIGHT = getHead("§r§lRitter", "General404");

    private static ItemStack getHead(String displayname, String username) {
        ItemStack item = new ItemStack(Material.SKULL_ITEM, 1, (short) 3);
        SkullMeta meta = (SkullMeta) item.getItemMeta();
        meta.setOwner(username);
        meta.setDisplayName(displayname);
        item.setItemMeta(meta);
        return item;
    }

}
