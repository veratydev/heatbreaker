package de.veraty.heartbreaker.utils;

import lombok.AllArgsConstructor;
import lombok.Builder;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.inventory.meta.LeatherArmorMeta;

import java.util.*;

@AllArgsConstructor
@Builder
public class ItemBuilder {

    public static ItemStack NULL = new ItemBuilder().spacer().build();
    public static ItemStack SETUP = new ItemBuilder().setMaterial(Material.GOLD_PICKAXE).setName("§r§lSetup").build();
    public static ItemStack WAND = new ItemBuilder().setMaterial(Material.BLAZE_ROD).setName("§r§lHeartbreaker").addLore("§eLinksklick um zum schiessen").addLore("§eEs gibt eine Sekunde Cooldown").build();
    public static ItemStack GRENADE = new ItemBuilder().setMaterial(Material.FIREBALL).setName("§r§lGranate").addLore("§eLinksklick um zum werfen").addLore("§eEs gibt eine Sekunde Cooldown").build();
    public static ItemStack SHOP = new ItemBuilder().setMaterial(Material.ENDER_CHEST).setName("§r§lShop").addLore("§eDu kannst Fähigkeiten kaufen").addLore("§eund Items kaufen sowie aufwerten").build();
    public static ItemStack ABILITY_NONE = new ItemBuilder().setMaterial(Material.BARRIER).setName("§r§lFähigkeit").addLore("§eMomentan ist keine Fähigkeit ausgewählt").addLore("§eDu kannst Fähigkeiten im Shop kaufen").build();
    public static ItemStack ARENA = new ItemBuilder().setMaterial(Material.NETHER_STAR).setName("§r§lArenen").addLore("§eDu kannst eine Arena auswählen").build();
    private Material material;
    private int amount = 1, data = 0;
    private Color color;
    private boolean unbreakable;
    private List<String> lore;
    private List<ItemFlag> itemFlags;
    private String name;
    private Map<Enchantment, Integer> enchantments;

    public ItemBuilder() {
        this.material = Material.BARRIER;
        this.amount = 1;
        this.data = 0;
        this.unbreakable = false;
        this.lore = new ArrayList<String>();
        this.itemFlags = new ArrayList<ItemFlag>();
        this.name = null;
        this.color = null;
        this.enchantments = new HashMap<Enchantment, Integer>();
    }

    public ItemBuilder addFlag(ItemFlag flag) {
        if (itemFlags != null) itemFlags.add(flag);
        return this;
    }

    public ItemBuilder addLore(String string) {
        if (lore != null) lore.add(string);
        return this;
    }

    public ItemBuilder addEnchantment(Enchantment enchantment, int level) {
        if (enchantments != null) enchantments.put(enchantment, level);
        return this;
    }

    public ItemBuilder setColor(Color color) {
        this.color = color;
        return this;
    }

    public ItemBuilder setAmount(int amount) {
        this.amount = amount;
        return this;
    }

    public ItemBuilder setData(int data) {
        this.data = data;
        return this;
    }

    public ItemBuilder setEnchantments(Map<Enchantment, Integer> enchantments) {
        this.enchantments = enchantments;
        return this;
    }

    public ItemBuilder setItemFlags(List<ItemFlag> itemFlags) {
        this.itemFlags = itemFlags;
        return this;
    }

    public ItemBuilder setLore(List<String> lore) {
        this.lore = lore;
        return this;
    }

    public ItemBuilder setMaterial(Material material) {
        this.material = material;
        return this;
    }

    public ItemBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public ItemBuilder setUnbreakable(boolean unbreakable) {
        this.unbreakable = unbreakable;
        return this;
    }

    public ItemStack build() {
        if (material == null) return null;
        ItemStack itemStack = new ItemStack(material, amount, (short) data);
        ItemMeta itemMeta = itemStack.getItemMeta();

        if (itemFlags != null)
            for (ItemFlag flag : itemFlags)
                itemMeta.addItemFlags(flag);

        if (lore != null)
            itemMeta.setLore(lore);

        if (name != null)
            itemMeta.setDisplayName(name);

        if (unbreakable)
            itemMeta.spigot().setUnbreakable(true);

        if (itemMeta instanceof LeatherArmorMeta && color != null)
            ((LeatherArmorMeta) itemMeta).setColor(color);

        itemStack.setItemMeta(itemMeta);

        return itemStack;
    }

    public ItemBuilder spacer() {
        this.material = Material.STAINED_GLASS_PANE;
        this.data = 8;
        this.amount = 1;
        this.name = "§r";
        return this;
    }

    public static ItemStack rename(ItemStack item, String name) {
        ItemMeta meta = item.getItemMeta();
        meta.setDisplayName(name);
        item.setItemMeta(meta);
        return item;
    }


    public static ItemStack lore(ItemStack item, String lore) {
        ItemMeta meta = item.getItemMeta();
        meta.setLore(Arrays.asList(lore));
        item.setItemMeta(meta);
        return item;
    }
}