package de.veraty.heartbreaker;

import de.veraty.heartbreaker.commands.ArenaCommand;
import de.veraty.heartbreaker.commands.HeartBreakerCommand;
import de.veraty.heartbreaker.game.Game;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.arena.ArenaManager;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.helper.impl.AbilityInventoryOpener;
import de.veraty.heartbreaker.helper.impl.ArenasInventoryOpener;
import de.veraty.heartbreaker.helper.impl.shop.CostumeInventoryOpener;
import de.veraty.heartbreaker.helper.impl.shop.ItemInventoryOpener;
import de.veraty.heartbreaker.helper.impl.shop.ShopAbilityInventoryOpener;
import de.veraty.heartbreaker.helper.impl.shop.ShopInventoryOpener;
import de.veraty.heartbreaker.listener.block.BlockBreakListener;
import de.veraty.heartbreaker.listener.block.BlockPlaceListener;
import de.veraty.heartbreaker.listener.entity.EntityDamageByEntityListener;
import de.veraty.heartbreaker.listener.entity.EntityDamageListener;
import de.veraty.heartbreaker.listener.entity.FoodLevelChangeListener;
import de.veraty.heartbreaker.listener.inventory.InventoryClickListener;
import de.veraty.heartbreaker.listener.inventory.InventoryCloseListener;
import de.veraty.heartbreaker.listener.player.*;
import de.veraty.heartbreaker.listener.world.ThunderChangeListener;
import de.veraty.heartbreaker.listener.world.WeatherChangeListener;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.List;

public class HeartBreaker extends JavaPlugin {

    private static HeartBreaker instance;

    private static final @Getter
    @Setter
    String prefix = "§7»§c§l Heartbreaker §8● §r§7", noPermission = prefix + "§cDu hast dazu keine Berechtigung", scoreboard = "§7»§c§l Heartbreaker";

    private @Getter
    @Setter
    Game game;

    private @Getter
    @Setter
    ArenaManager arenaManager;

    private @Getter
    @Setter
    List<InventoryOpener> inventoryOpeners;

    public HeartBreaker() {

        instance = this;
        this.inventoryOpeners = new ArrayList<InventoryOpener>();
        this.arenaManager = new ArenaManager();
        this.game = new Game();

        if (arenaManager.getLoadedArenas().size() > 0)
            game.setArena(arenaManager.getLoadedArenas().get(0));


    }

    @Override
    public void onEnable() {

        register();

        new BukkitRunnable() {
            @Override
            public void run() {
                game.update();
            }
        }.runTaskTimerAsynchronously(this, 1, 1);


        for (Player player : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(player);
            if (HeartBreaker.getInstance().getArenaManager().getLoadedArenas().size() > 0) {
                user.onSpawn(HeartBreaker.getInstance().getArenaManager().getLoadedArenas().get(0));
            }
        }

    }

    private void register() {

        getCommand("heartbreaker").setExecutor(new HeartBreakerCommand());
        getCommand("arena").setExecutor(new ArenaCommand());

        new BlockBreakListener().register();
        new BlockPlaceListener().register();
        new EntityDamageByEntityListener().register();
        new EntityDamageListener().register();
        new FoodLevelChangeListener().register();
        new InventoryClickListener().register();
        new InventoryCloseListener().register();
        new AsyncPlayerChatListener().register();
        new PlayerAchievementAwardedListener().register();
        new PlayerDropItemListener().register();
        new PlayerInteractListener().register();
        new PlayerJoinListener().register();
        new PlayerKickListener().register();
        new PlayerPickupItemListener().register();
        new PlayerQuiListener().register();
        new ThunderChangeListener().register();
        new WeatherChangeListener().register();
        new PlayerToggleFlightListener().register();

        this.inventoryOpeners.add(new AbilityInventoryOpener());
        this.inventoryOpeners.add(new ArenasInventoryOpener());
        this.inventoryOpeners.add(new ShopInventoryOpener());
        this.inventoryOpeners.add(new ItemInventoryOpener());
        this.inventoryOpeners.add(new ShopAbilityInventoryOpener());
        this.inventoryOpeners.add(new CostumeInventoryOpener());

    }

    @Override
    public void onDisable() {
        super.onDisable();
    }

    public static HeartBreaker getInstance() {
        return instance;
    }

}

