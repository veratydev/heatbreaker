package de.veraty.heartbreaker.helper;

import de.veraty.heartbreaker.game.User;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public interface InventoryOpener {

    public Inventory getInventory();
    public boolean matches(Inventory inventory);
    public void onClick(InventoryClickEvent event);
    public void onClose(User user);

}
