package de.veraty.heartbreaker.helper.impl.shop;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.utils.Heads;
import de.veraty.heartbreaker.utils.ItemBuilder;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class ShopInventoryOpener implements InventoryOpener {

    private final @Getter
    Inventory inventory;

    private static ShopInventoryOpener instance;

    public ShopInventoryOpener() {
        inventory = Bukkit.createInventory(null, 9 * 3, "§7§lShop");
        inventory.setItem(9 + 2, new ItemBuilder().setMaterial(Material.FIREBALL).setName("§r§lItems").build());
        inventory.setItem(9 + 4, new ItemBuilder().setMaterial(Material.FEATHER).setName("§r§lFähigkeiten").build());
        inventory.setItem(9 + 6, ItemBuilder.rename(Heads.DWARF.clone(), "§r§lKostüme"));
        instance = this;
    }

    @Override
    public void onClose(User user) {
        user.playSound(Sound.CHEST_CLOSE);
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        Material type = event.getCurrentItem().getType();
        switch (type) {
            case FIREBALL:
                event.getWhoClicked().openInventory(ItemInventoryOpener.getInstance().getInventory());
                break;
            case FEATHER:
                event.getWhoClicked().openInventory(ShopAbilityInventoryOpener.getInstance().getInventory());
                break;
            case SKULL_ITEM:
                event.getWhoClicked().openInventory(CostumeInventoryOpener.getInstance().getInventory());
                break;
        }
    }


    @Override
    public boolean matches(Inventory inventory) {
        return inventory.getTitle().equals(this.inventory.getTitle());
    }

    public static ShopInventoryOpener getInstance() {
        return instance;
    }
}


