package de.veraty.heartbreaker.helper.impl.shop;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.scoreboard.ScoreboardManager;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.utils.ItemBuilder;
import lombok.Getter;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.Arrays;
import java.util.List;

public class ItemInventoryOpener implements InventoryOpener {

    private final @Getter
    Inventory inventory;

    private static ItemInventoryOpener instance;
    private List<ShopItem> shopItems;

    public ItemInventoryOpener() {
        this.shopItems = Arrays.asList(ShopItem.GRENADE);
        inventory = Bukkit.createInventory(null, 9 * 3, "§7§lShop > Kostüme");
        for (int i = 0, j = 10; i < shopItems.size(); i++, j++) {
            ShopItem item = shopItems.get(i);
            inventory.setItem(j, ItemBuilder.lore(item.getItemStack().clone(), "§rPreis: §4 " + item.getPrice() + "❤"));
        }
        instance = this;
    }

    @Override
    public void onClose(User user) {
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        User user = User.getInstance((Player) event.getWhoClicked());
        String name = event.getCurrentItem().getItemMeta().getDisplayName();

        if (event.getCurrentItem().getType() == Material.BARRIER) {
            user.setHelmet(null);
            user.setChestplate(null);
            user.setLeggings(null);
            user.setBoots(null);
            user.cloth();
            return;
        }

        for (ShopItem item : shopItems) {
            if (item.getItemStack().getItemMeta().getDisplayName().equals(name)) {
                if (user.getHearts() < item.getPrice()) {
                    user.playSound(Sound.ITEM_BREAK);
                    user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "§cDu hast zu wenig Herzen gebrochen");
                } else {
                    user.playSound(Sound.ITEM_PICKUP);
                    user.setHearts(user.getHearts() - item.getPrice());
                    ScoreboardManager.updateSidebar(user, HeartBreaker.getScoreboard());
                    user.getPlayer().getInventory().addItem(item.getItemStack());
                    user.getPlayer().closeInventory();
                    user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast das Item gekauft");
                }
                return;
            }
        }

    }


    @Override
    public boolean matches(Inventory inventory) {
        return inventory.getTitle().equals(this.inventory.getTitle());
    }

    public static ItemInventoryOpener getInstance() {
        return instance;
    }
}


