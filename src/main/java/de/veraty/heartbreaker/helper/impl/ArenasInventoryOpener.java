package de.veraty.heartbreaker.helper.impl;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.arena.Arena;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

import java.util.ArrayList;
import java.util.List;

public class ArenasInventoryOpener implements InventoryOpener {

    private static final String TITLE = "§7§lArenas";

    private static ArenasInventoryOpener instance;

    public ArenasInventoryOpener() {
        instance = this;
    }

    @Override
    public Inventory getInventory() {
        Inventory inventory = Bukkit.createInventory(null, 9 * 3, TITLE);

        for (Arena arena : getArenas()) {
            inventory.addItem(new ItemBuilder().setMaterial(arena.isPrivateArena() ? Material.IRON_BLOCK : Material.GOLD_BLOCK).setName("§r§l" + arena.getName()).addLore("§e§o" + arena.getId()).addLore("§e§l" + arena.getUsers().size() + "§r§l Spieler").build());
        }
        return inventory;
    }

    private List<Arena> getArenas() {
        List<Arena> arenas = new ArrayList<Arena>();
        arenas.addAll(HeartBreaker.getInstance().getArenaManager().getLoadedArenas());
        for (Player player : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(player);
            if (user.getArena() != null && !arenas.contains(user.getArena())) {
                if (user.getArena().getUsers().size() > 0)
                    arenas.add(user.getArena());
            }

        }
        return arenas;
    }

    @Override
    public boolean matches(Inventory inventory) {
        return inventory.getTitle().equals(TITLE);
    }

    @Override
    public void onClose(User user) {
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        if (event.getCurrentItem().getType() == Material.GOLD_BLOCK) {
            if (event.getCurrentItem().getItemMeta().getLore() != null && event.getCurrentItem().getItemMeta().getLore().size() > 0) {
                String id = event.getCurrentItem().getItemMeta().getLore().get(0);
                for (Arena arena : getArenas()) {
                    if (arena.getId().equalsIgnoreCase(ChatColor.stripColor(id))) {
                        User user = User.getInstance((Player) event.getWhoClicked());
                        user.onSpawn(arena);
                        user.playSound(Sound.ITEM_PICKUP);
                    }
                }
            }
        }
    }

    public static ArenasInventoryOpener getInstance() {
        return instance;
    }
}
