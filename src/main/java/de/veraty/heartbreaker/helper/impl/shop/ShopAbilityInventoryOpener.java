package de.veraty.heartbreaker.helper.impl.shop;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.Ability;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.scoreboard.ScoreboardManager;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class ShopAbilityInventoryOpener implements InventoryOpener {

    private static ShopAbilityInventoryOpener instance;

    private Inventory inventory;

    public ShopAbilityInventoryOpener() {
        inventory = Bukkit.createInventory(null, 9 * 3, "§7§lShop > Fähigkeiten");
        inventory.setItem(9 + 1, ItemBuilder.lore(Ability.DOUBLE_JUMP.getIcon(), "§rPreis: §4 " + Ability.DOUBLE_JUMP.getPrice() + "❤"));
        inventory.setItem(9 + 2, ItemBuilder.lore(Ability.REGENERATION.getIcon(), "§rPreis: §4 " + Ability.REGENERATION.getPrice() + "❤"));
        inventory.setItem(9 + 3, ItemBuilder.lore(Ability.NOKNOCKBACK.getIcon(), "§rPreis: §4 " + Ability.NOKNOCKBACK.getPrice() + "❤"));
        inventory.setItem(9 + 4, ItemBuilder.lore(Ability.FASTSHOTS.getIcon(), "§rPreis: §4 " + Ability.FASTSHOTS.getPrice() + "❤"));
        instance = this;
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        String name = event.getCurrentItem().getItemMeta().getDisplayName();
        User user = User.getInstance((Player) event.getWhoClicked());
        for (Ability ability : Ability.values()) {
            if (ability.getIcon().getItemMeta().getDisplayName().equalsIgnoreCase(name)) {
                if (user.getHearts() < ability.getPrice()) {
                    user.playSound(Sound.ITEM_BREAK);
                    user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "§cDu hast zu wenig Herzen gebrochen");
                } else {
                    user.playSound(Sound.ITEM_PICKUP);
                    user.setHearts(user.getHearts() - ability.getPrice());
                    ScoreboardManager.updateSidebar(user, HeartBreaker.getScoreboard());
                    user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "§Du hast die Fähigkeit gekauft");
                }
                return;
            }
        }
    }

    @Override
    public void onClose(User user) {
        user.playSound(Sound.CHEST_CLOSE);
    }

    @Override
    public boolean matches(Inventory inventory) {
        return inventory.getTitle().equals(this.inventory.getTitle());
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public static ShopAbilityInventoryOpener getInstance() {
        return instance;
    }
}
