package de.veraty.heartbreaker.helper.impl;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.Ability;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.utils.ItemBuilder;
import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;

public class AbilityInventoryOpener implements InventoryOpener {

    private static AbilityInventoryOpener instance;

    private Inventory inventory;

    public AbilityInventoryOpener() {
        inventory = Bukkit.createInventory(null, 9 * 3, "§r§7Fähigkeiten");
        inventory.setItem(9 + 1, new ItemBuilder().setMaterial(Material.BARRIER).setName("§r").build());
        inventory.setItem(9 + 3, Ability.DOUBLE_JUMP.getIcon());
        inventory.setItem(9 + 4, Ability.REGENERATION.getIcon());
        inventory.setItem(9 + 5, Ability.NOKNOCKBACK.getIcon());
        inventory.setItem(9 + 6, Ability.FASTSHOTS.getIcon());

        instance = this;
    }

    @Override
    public void onClick(InventoryClickEvent event) {
        String name = event.getCurrentItem().getItemMeta().getDisplayName();
        User user = User.getInstance((Player) event.getWhoClicked());
        for (Ability ability : Ability.values()) {
            if (ability.getIcon().getItemMeta().getDisplayName().equalsIgnoreCase(name)) {
                user.setAbility(ability);
                user.giveItems();
                user.playSound(Sound.ITEM_PICKUP);
                user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die Fähigkeit §e" + ability.getName() + "§7 gewählt");
                if (ability == Ability.DOUBLE_JUMP)
                    user.getPlayer().setAllowFlight(true);
                else user.getPlayer().setAllowFlight(false);
                user.getPlayer().closeInventory();
                return;
            }
        }
        if (event.getCurrentItem().getType() == Material.BARRIER) {
            user.setAbility(Ability.NONE);
            user.giveItems();
            user.playSound(Sound.ITEM_PICKUP);
            user.getPlayer().setAllowFlight(false);
            user.getPlayer().closeInventory();
        }
    }

    @Override
    public void onClose(User user) {
        user.playSound(Sound.CHEST_CLOSE);
    }

    @Override
    public boolean matches(Inventory inventory) {
        return inventory.getTitle().equals(this.inventory.getTitle());
    }

    @Override
    public Inventory getInventory() {
        return inventory;
    }

    public static AbilityInventoryOpener getInstance() {
        return instance;
    }
}
