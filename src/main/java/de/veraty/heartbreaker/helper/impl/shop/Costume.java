package de.veraty.heartbreaker.helper.impl.shop;

import de.veraty.heartbreaker.game.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.inventory.ItemStack;

public class Costume extends ShopItem {

    private @Getter
    @Setter
    ItemStack helmet, chestplate, leggings, boots;

    public Costume(String id, ItemStack helmet, ItemStack chestplate, ItemStack leggings, ItemStack boots, int price) {
        super(id, helmet, price);
        this.helmet = helmet;
        this.chestplate = chestplate;
        this.leggings = leggings;
        this.boots = boots;
    }

    public void cloth(User user) {
        user.setHelmet(helmet);
        user.setChestplate(chestplate);
        user.setLeggings(leggings);
        user.setBoots(boots);
        user.cloth();
    }
}
