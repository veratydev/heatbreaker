package de.veraty.heartbreaker.helper.impl.shop;

import de.veraty.heartbreaker.utils.Heads;
import de.veraty.heartbreaker.utils.ItemBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Color;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

@AllArgsConstructor
public class ShopItem {
    public static ShopItem GRENADE = new ShopItem("#G", ItemBuilder.GRENADE, 10);
    public static Costume
            DWARF = new Costume("#DWARF", Heads.DWARF,
            new ItemBuilder().setMaterial(Material.LEATHER_CHESTPLATE).setColor(Color.LIME).setName("§r§lJacke").build(),
            new ItemBuilder().setMaterial(Material.LEATHER_LEGGINGS).setColor(Color.LIME).setName("§r§lHose").build(),
            new ItemBuilder().setMaterial(Material.LEATHER_BOOTS).setColor(Color.fromRGB(135, 65, 25)).setName("§r§lSchuhe").build(), 200),
            PIRATE = new Costume("#PIRATE", Heads.PIRATE,
                    new ItemBuilder().setMaterial(Material.LEATHER_CHESTPLATE).setColor(Color.fromRGB(135, 65, 25)).setName("§r§lJacke").build(),
                    new ItemBuilder().setMaterial(Material.LEATHER_LEGGINGS).setColor(Color.BLACK).setName("§r§lHose").build(),
                    new ItemBuilder().setMaterial(Material.LEATHER_BOOTS).setColor(Color.fromRGB(135, 65, 25)).setName("§r§lSchuhe").build(), 300),
            KNIGHT = new Costume("#KNIGHT", Heads.KNIGHT,
                    new ItemBuilder().setMaterial(Material.IRON_CHESTPLATE).setName("§r§lBrustpanzer").build(),
                    new ItemBuilder().setMaterial(Material.CHAINMAIL_LEGGINGS).setName("§r§lHose").build(),
                    new ItemBuilder().setMaterial(Material.IRON_BOOTS).setName("§r§lStiefel").build(), 400),
            MINION = new Costume("#MINION", Heads.MINION,
                    new ItemBuilder().setMaterial(Material.LEATHER_CHESTPLATE).setColor(Color.YELLOW).setName("§r§lOberkörper").build(),
                    new ItemBuilder().setMaterial(Material.LEATHER_LEGGINGS).setColor(Color.BLUE).setName("§r§lHose").build(),
                    new ItemBuilder().setMaterial(Material.LEATHER_BOOTS).setColor(Color.fromRGB(135, 65, 25)).setName("§r§lSchuhe").build(), 500);

    private @Getter
    @Setter
    String id;
    private @Getter
    @Setter
    ItemStack itemStack;
    private @Getter
    @Setter
    int price;

}
