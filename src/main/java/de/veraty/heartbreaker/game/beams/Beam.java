package de.veraty.heartbreaker.game.beams;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.items.WandItem;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.List;

public interface Beam {

    public void update();

    public WandItem getWandItem();

    public Location getLocation();

    public Vector getDirection();

    public float getDistance();

    public void setDistance(float distance);

    public default void destroy() {
        new BukkitRunnable() {
            @Override
            public void run() {
                List<Beam> beams = HeartBreaker.getInstance().getGame().getBeams();
                if (beams.contains(Beam.this))
                    beams.remove(Beam.this);
            }
        }.runTask(HeartBreaker.getInstance());

    }


    public default void spawn() {
        new BukkitRunnable() {
            @Override
            public void run() {
                List<Beam> beams = HeartBreaker.getInstance().getGame().getBeams();
                if (!beams.contains(Beam.this))
                    beams.add(Beam.this);
            }
        }.runTask(HeartBreaker.getInstance());

    }
}
