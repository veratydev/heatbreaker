package de.veraty.heartbreaker.game.beams.impl;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.beams.Beam;
import de.veraty.heartbreaker.game.items.GrenadeItem;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.Sound;
import org.bukkit.util.Vector;

public class GrenadeBeam implements Beam {

    private final @Getter
    GrenadeItem wandItem;
    private final @Getter
    Vector direction;
    private final @Getter
    Location start;
    private @Getter
    @Setter
    float distance;

    private Vector gravity;
    private float speed;

    public GrenadeBeam(GrenadeItem wandItem, Vector direction, Location location) {
        this.wandItem = wandItem;
        this.direction = direction.setY(direction.getY()+2);
        this.start = location.add(direction.clone().multiply(2));
        this.distance = 1;
        this.gravity = new Vector(0, -3, 0);
        this.speed = 5f;
    }

    public Vector bullet(Vector s, Vector v, Vector g, final float speed, final float i) {
        return s.add(v.multiply((float) speed * i).add(g.multiply((float) speed * i)));
    }

    @Override
    public void update() {
        distance += 1f;
        Location location = bullet(start.clone().toVector(), direction.clone(), gravity.clone(), 0.7f, distance).toLocation(this.start.getWorld());

        if (HeartBreaker.getInstance().getGame().isSpawnArea(location.toVector())) {
            destroy();
            return;
        }

        try {
            if (distance > wandItem.getReach() || location.getBlock().getType().isSolid()) {
                for (User user : wandItem.getUser().getArena().getUsers()) {
                    if (user.getPlayer().getLocation().distanceSquared(location) < wandItem.getRadius()) {
                        user.hit(this);
                        wandItem.getUser().onHit(user);
                    }
                }
                spawnExplosion(location);
                location.getWorld().playSound(location, Sound.EXPLODE, 1, 1);
                destroy();
            } else {
                spawnParticle(location);
            }
        } catch (Exception e) {
            destroy();
        }
    }

    public void spawnExplosion(Location location, User entity) {
        for (double i = 0; i <= Math.PI; i += Math.PI / 10) {
            double radius = Math.sin(i) * wandItem.getExplosion();
            double y = Math.cos(i) * wandItem.getExplosion();
            for (double a = 0; a < Math.PI * 2; a += Math.PI / 10) {
                double x = Math.cos(a) * radius;
                double z = Math.sin(a) * radius;
                location.add(x, y, z);
                spawnParticle(location, entity);
                location.subtract(x, y, z);
            }
        }
    }

    public void spawnExplosion(Location location) {
        for (double i = 0; i <= Math.PI; i += Math.PI / 10) {
            double radius = Math.sin(i) * wandItem.getExplosion();
            double y = Math.cos(i) * wandItem.getExplosion();
            for (double a = 0; a < Math.PI * 2; a += Math.PI / 10) {
                double x = Math.cos(a) * radius;
                double z = Math.sin(a) * radius;
                location.add(x, y, z);
                spawnParticle(location);
                location.subtract(x, y, z);
            }
        }
    }

    public void spawnParticle(Location location) {
        for (User user : wandItem.getUser().getArena().getUsers()) {
            user.getPlayer().playEffect(location, Effect.HEART, 1);
        }
    }

    public void spawnParticle(Location location, User entity) {
        for (User user : wandItem.getUser().getArena().getUsers()) {
            if (user != entity)
                user.getPlayer().playEffect(location, Effect.HEART, 1);
        }
    }


    @Override
    public Location getLocation() {
        return start;
    }
}
