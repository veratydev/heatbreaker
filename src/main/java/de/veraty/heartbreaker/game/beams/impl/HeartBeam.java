package de.veraty.heartbreaker.game.beams.impl;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.beams.Beam;
import de.veraty.heartbreaker.game.items.WandItem;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Effect;
import org.bukkit.Location;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

public class HeartBeam implements Beam {

    private final @Getter
    WandItem wandItem;
    private final @Getter
    Vector direction;
    private final @Getter
    Location location;
    private @Getter
    @Setter
    float distance;

    public HeartBeam(WandItem wandItem, Vector direction, Location location) {
        this.wandItem = wandItem;
        this.direction = direction;
        this.location = location;
        this.distance = 0.5f;
    }

    @Override
    public void update() {
        for (int i = 0; i < wandItem.getSpeed(); i++) {
            distance += 0.1f;
            Vector velocity = direction.clone().multiply(Double.valueOf(distance));
            location.add(velocity);

            if (distance < wandItem.getReach()) {

            /* Users should'nt shoot into the Spawn Area */
                try {
                    if (HeartBreaker.getInstance().getGame().isSpawnArea(location.toVector()) || location.getBlock().getType().isSolid()) {
                        destroy();
                        return;
                    }
                } catch (Exception e) {
                    destroy();
                }

                new BukkitRunnable() {
                    @Override
                    public void run() {
                        for (User enemy : wandItem.getUser().getArena().getUsers()) {
                            if (enemy != wandItem.getUser()) {
                                if (enemy.getPlayer().getLocation().distanceSquared(location) < wandItem.getRadius()) {
                                    enemy.hit(HeartBeam.this);
                                    wandItem.getUser().onHit(enemy);
                                    spawnExplosion(location, enemy);
                                    destroy();
                                    break;
                                }
                            }
                        }

                        spawnParticle(location);

                    }
                }.runTask(HeartBreaker.getInstance());
            } else {
                destroy();
            }
        }
    }

    public void spawnExplosion(Location location, User entity) {
        for (double i = 0; i <= Math.PI; i += Math.PI / 10) {
            double radius = Math.sin(i) * wandItem.getExplosion();
            double y = Math.cos(i) * wandItem.getExplosion();
            for (double a = 0; a < Math.PI * 2; a += Math.PI / 10) {
                double x = Math.cos(a) * radius;
                double z = Math.sin(a) * radius;
                location.add(x, y, z);
                spawnParticle(location, entity);
                location.subtract(x, y, z);
            }
        }
    }

    public void spawnParticle(Location location) {
        for (User user : wandItem.getUser().getArena().getUsers()) {
            user.getPlayer().playEffect(location, Effect.HEART, 1);
        }
    }

    public void spawnParticle(Location location, User entity) {
        for (User user : wandItem.getUser().getArena().getUsers()) {
            if (user != entity)
                user.getPlayer().playEffect(location, Effect.HEART, 1);
        }
    }
}
