package de.veraty.heartbreaker.game;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.arena.Arena;
import de.veraty.heartbreaker.game.beams.Beam;
import lombok.Getter;
import lombok.Setter;
import net.minecraft.server.v1_8_R3.IChatBaseComponent;
import net.minecraft.server.v1_8_R3.PacketPlayOutChat;
import org.bukkit.Bukkit;
import org.bukkit.craftbukkit.v1_8_R3.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Game {

    private static String[] killMessages = new String[]{
            "%1$s hat %2$s das Herz gebrochen",
            "%1$s hat mit %2$s per SMS Schluss gemacht",
            "%2$s wurde von %1$s abserviert",
            "%2$s kann %1$s niemehr verzeihen",
            "%2$s brauch dank %1$s nun viel Schokolade",
            "%1$s ist ein echter Herzensbrecher",
            "%2$s wurde abserviert",
            "%2$s's Herz ist  gebrochen",
            "%1$s hat %2$s einen Korb gegeben",
            "%1$s hat %2$s von sich geschoben",
            "%2$s wurde von %1$s stark enttäuscht",
            "%2$s fühlt sich von %1$s im Stich gelassen",
            "%1$s fühlte sich bei %2$s nicht mehr wohl",
            "%2$s ist wohl doch nicht der richtige Partner für %1$s",
            "%1$s hat %2$s inflagranti erwischt",
            "%1$s hat %2$s einen Single-Ratgeber geschenkt",
            "%1$s hat den Facebook-Status zu single geändert",
            "%1$s hat %2$s den neuen Partner vorgestellt",
            "%1$s hat %2$s beim Fremdknutschen erwischt",
            "%1$s ist aufgefallen dass er/sie doch vom anderen Ufer ist",
            "%2$s braucht erstmal eine Beziehungs Pause",
            "%1$s kann sich %2$s nicht mehr schön trinken",
            "%1$s hat nun keine Liebesbriefe mehr für %2$s"
    };


    private @Getter
    @Setter
    List<Beam> beams;

    private @Getter
    @Setter
    Arena arena;

    public Game() {
        beams = new ArrayList<Beam>();
    }

    public void update() {
        for (Beam beam : beams) {
            beam.update();
        }
        for (Player player : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(player);
            if (user.getAbility() == Ability.REGENERATION) {
                if (!isSpawnArea(user.getPlayer().getLocation().toVector())) {
                    new BukkitRunnable() {
                        @Override
                        public void run() {
                            user.setHealth(user.getHealth() + 0.004f);
                            if (user.getHealth() > user.getMaxHealth())
                                user.setHealth(user.getMaxHealth());
                            user.updateHeath();
                        }
                    }.runTask(HeartBreaker.getInstance());
                }
            }
            long cooldown = user.getHitCooldown();
            long maxCooldown = user.getMaxHitCooldown();
            if (cooldown > maxCooldown) cooldown = maxCooldown;
            float percent = ((float) cooldown / maxCooldown);
            String text = "";
            if (percent < 1) {
                float x = percent * 10;
                for (int i = 0; i < 10; i++) {
                    if (x >= i)
                        text += "§c⚊";
                    else if (x >= i - 1) text += "§7⚊";
                    else text += "§8⚊";
                }
            }
            IChatBaseComponent cbc = IChatBaseComponent.ChatSerializer.a("{\"text\": \"" + text + "\"}");
            PacketPlayOutChat ppoc = new PacketPlayOutChat(cbc, (byte) 2);
            ((CraftPlayer) player).getHandle().playerConnection.sendPacket(ppoc);
        }
    }

    public boolean isSpawnArea(Vector vector) {
        if (arena == null)
            return false;
        else return arena.getSpawnRegion().contains(vector);
    }

    public String getArenaName() {
        if (arena == null || arena.getName() == null)
            return "None";
        return arena.getName();
    }

    public static String getKillMessage(String killer, String entity) {
        int index = ThreadLocalRandom.current().nextInt(killMessages.length);
        return String.format(killMessages[index], killer, entity);
    }

    public void sendArenaMessage(String message, User user) {
        user.getArena().broadcastMessage(message);
    }

    public static List<Arena> getArenas() {
        List<Arena> arenas = new ArrayList<Arena>();
        arenas.addAll(HeartBreaker.getInstance().getArenaManager().getLoadedArenas());
        for (Player player : Bukkit.getOnlinePlayers()) {
            User user = User.getInstance(player);
            if (user.getArena() != null && !arenas.contains(user.getArena())) {
                if (user.getArena().getUsers().size() > 0)
                    arenas.add(user.getArena());
            }

        }
        return arenas;
    }

    public static Arena findPublicArena() {
        Arena arena = null;
        for (Arena all : getArenas()) {
            if (!all.isPrivateArena()) {
                arena = all;
                break;
            }
        }
        return arena;
    }

}
