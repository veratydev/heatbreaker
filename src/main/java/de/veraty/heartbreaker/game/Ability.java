package de.veraty.heartbreaker.game;

import de.veraty.heartbreaker.utils.ItemBuilder;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

@AllArgsConstructor
public enum Ability {

    NONE(ItemBuilder.ABILITY_NONE, "None", 0),
    DOUBLE_JUMP(new ItemBuilder().setMaterial(Material.FEATHER).setName("§r§lDoppelsprung").addLore("§eDu kannst zweimal springen").build(), "Doppelsprung", 500),
    REGENERATION(new ItemBuilder().setMaterial(Material.INK_SACK).setData(1).setName("§r§lRegeneration").addLore("§eDu regenerierst dauerhaft").build(), "Regeneration", 1500),
    NOKNOCKBACK(new ItemBuilder().setMaterial(Material.ANVIL).setName("§r§lStandhaft").addLore("§eDu bekommst weniger Rückstoss").addLore("§eund 75% des Schadens").build(), "Standhaft", 2000),
    FASTSHOTS(new ItemBuilder().setMaterial(Material.FIREWORK).setName("§r§lSchnellschuss").addLore("§eDu kannst 3x so schnell schiessen").build(), "Schnellschuss", 3000);


    private @Getter
    ItemStack icon;

    private @Getter
    String name;

    private @Getter
    int price;

}
