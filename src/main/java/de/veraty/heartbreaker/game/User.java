package de.veraty.heartbreaker.game;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.arena.Arena;
import de.veraty.heartbreaker.game.arena.ArenaSetup;
import de.veraty.heartbreaker.game.beams.Beam;
import de.veraty.heartbreaker.game.items.GrenadeItem;
import de.veraty.heartbreaker.game.items.WandItem;
import de.veraty.heartbreaker.utils.ItemBuilder;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.GameMode;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.util.Vector;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class User {

    private static List<User> instances = new ArrayList<User>();

    private @Getter
    @Setter
    Scoreboard scoreboard;

    private @Getter
    @Setter
    long beamCooldown, jumpCooldown, grenadeCooldown;
    private @Getter
    @Setter
    int maxBeamCooldown = 1000, fastShootCooldown = 300, maxJumpCooldown = 1000, maxGrenadeCooldown = 3000;
    private @Getter
    @Setter
    Player player;
    private @Getter
    @Setter
    Arena arena;
    private @Getter
    @Setter
    int coins, kills, hearts;
    private @Getter
    @Setter
    Ability ability;
    private @Getter
    @Setter
    float health, maxHealth;
    private @Getter
    @Setter
    ArenaSetup setup;
    private @Getter
    @Setter
    WandItem wandItem;
    private @Getter
    @Setter
    GrenadeItem grenadeItem;
    private @Getter
    @Setter
    ItemStack helmet, chestplate, leggings, boots;

    private final @Getter
    List<UUID> arenaInvitations;

    private User(Player player) {
        this.player = player;
        this.coins = 0;
        this.kills = 0;
        this.maxHealth = 1;
        this.health = maxHealth;
        this.wandItem = new WandItem(this, 30f, 4, 2f, (float) 1 / 5, 2);
        this.grenadeItem = new GrenadeItem(this);
        this.ability = Ability.NONE;
        this.arenaInvitations = new ArrayList<UUID>();
    }

    public void onSpawn(Arena arena) {
        if (this.arena != null)
            this.arena.removeUser(this);
        this.arena = arena;
        arena.addUser(this);
        health = maxHealth;
        kills = 0;
        flush();
        player.teleport(arena.getSpawnLocation());
        arena.handleVisibility(player);
    }

    public void cloth() {
        player.getInventory().setHelmet(helmet);
        player.getInventory().setChestplate(chestplate);
        player.getInventory().setLeggings(leggings);
        player.getInventory().setBoots(boots);
    }

    public void flush() {
        player.setFlying(false);
        if (ability == Ability.DOUBLE_JUMP) {
            player.setAllowFlight(true);
        } else {
            player.setAllowFlight(false);
        }
        player.setHealth(player.getMaxHealth());
        player.setFoodLevel(20);
        player.setGameMode(GameMode.ADVENTURE);
        for (PotionEffect potionEffect : player.getActivePotionEffects()) {
            player.removePotionEffect(potionEffect.getType());
        }
        player.getInventory().clear();
        player.getInventory().setArmorContents(null);
        updateHeath();
        updateLevel();
        giveItems();
    }

    public void giveItems() {
        player.getInventory().setItem(0, ItemBuilder.WAND);
        player.getInventory().setItem(6, ability.getIcon());
        player.getInventory().setItem(7, ItemBuilder.ARENA);
        player.getInventory().setItem(8, ItemBuilder.SHOP);
    }

    public void onLeave() {
        if (arena != null) arena.removeUser(this);
        setScoreboard(null);
    }

    public void onHit(User user) {
        playSound(Sound.WITHER_SHOOT);
    }

    public void hit(Beam beam) {
        health -= beam.getWandItem().getDamage() * (ability == Ability.NOKNOCKBACK ? 0.75f : 1);
        if (health <= 0) {
            beam.getWandItem().getUser().onKill(this);
            die(beam);
        } else {
            if (ability != Ability.NOKNOCKBACK)
                player.setVelocity(beam.getDirection().clone().multiply(2).setY(2));
            else
                player.setVelocity(beam.getDirection().clone().multiply(0.6).setY(0.6));
        }
        updateLevel();
        updateHeath();
        playSound(Sound.WITHER_HURT);
    }

    public void die(Beam beam) {
        playSound(Sound.WITHER_HURT);
        health = maxHealth;
        kills = 0;
        updateLevel();
        updateHeath();
        player.teleport(arena.getSpawnLocation());
        player.setVelocity(new Vector(0, 0, 0));
    }

    public void onKill(User entity) {
        if (entity == this) return;

        playSound(Sound.ENDERDRAGON_GROWL);
        kills++;
        hearts++;
        health += maxHealth / 3;
        if (health > maxHealth)
            health = maxHealth;
        updateHeath();
        updateLevel();
        arenaMessage(HeartBreaker.getPrefix() + Game.getKillMessage(player.getDisplayName(), entity.getPlayer().getDisplayName()));
    }

    public void playSound(Sound sound, float volume) {
        player.playSound(player.getLocation().clone().add(player.getLocation().getDirection()), sound, volume, 1);
    }

    public void playSound(Sound sound) {
        playSound(sound, 1);
    }

    public void updateLevel() {
        player.setLevel(kills);
        de.veraty.heartbreaker.game.scoreboard.ScoreboardManager.updateSidebar(this, HeartBreaker.getScoreboard());
    }

    public void updateHeath() {
        player.setExp(health);
        de.veraty.heartbreaker.game.scoreboard.ScoreboardManager.updateSidebar(this, HeartBreaker.getScoreboard());
    }

    public void arenaMessage(String message) {
        HeartBreaker.getInstance().getGame().sendArenaMessage(message, this);
    }

    public static User getInstance(Player player) {
        for (User instance : instances) {
            if (instance.player.getUniqueId().equals(player.getUniqueId())) {
                instance.player = player;
                return instance;
            }
        }

        User instance = new User(player);
        instances.add(instance);
        return instance;
    }

    public boolean hasBeamCooldown() {
        return System.currentTimeMillis() - beamCooldown < (ability == Ability.FASTSHOTS ? fastShootCooldown : maxBeamCooldown);
    }

    public long getHitCooldown() {
        return System.currentTimeMillis() - beamCooldown;
    }

    public long getMaxHitCooldown() {
        int max = (ability == Ability.FASTSHOTS ? fastShootCooldown : maxBeamCooldown);
        return max;
    }

    public boolean hasJumpCooldown() {
        return System.currentTimeMillis() - jumpCooldown < maxJumpCooldown;
    }

    public boolean hasGrenadeCooldown() {
        return System.currentTimeMillis() - grenadeCooldown < maxGrenadeCooldown;
    }
}
