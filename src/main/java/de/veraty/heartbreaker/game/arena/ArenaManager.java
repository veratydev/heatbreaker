package de.veraty.heartbreaker.game.arena;

import de.veraty.heartbreaker.serialize.ObjectSerializer;
import de.veraty.heartbreaker.serialize.serializables.SerializeArena;
import lombok.Getter;
import org.bukkit.Bukkit;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

public class ArenaManager {

    private static File arenaFolder = new File("plugins/HeartBreaker/arenas");

    private final @Getter
    List<Arena> loadedArenas;

    public ArenaManager() {
        this.loadedArenas = new ArrayList<Arena>();
        load();
    }

    public void load() {
        loadedArenas.clear();

        if (!arenaFolder.exists()) arenaFolder.mkdirs();
        for (File file : arenaFolder.listFiles()) {
            if (!file.isDirectory()) {
                if (file.getName().endsWith(".arena")) {
                    try {
                        SerializeArena serializeArena = (SerializeArena) ObjectSerializer.deserialize(new FileInputStream(file));
                        if (serializeArena != null)
                            loadedArenas.add(serializeArena.convert());
                    } catch (Exception exception) {
                        exception.printStackTrace();
                        Bukkit.getLogger().log(Level.WARNING, "Die Datei " + file.getName() + " konnte nicht gelesen werden");
                    }
                }
            }
        }

        Bukkit.getLogger().log(Level.INFO, "[HeartBreaker] Geladene Arenen : " + loadedArenas.size());
    }

    public Arena getArena(String name) {
        for (Arena arena : loadedArenas) {
            if (arena != null)
                if (arena.getName().equalsIgnoreCase(name))
                    return arena;
        }
        return null;
    }

    public void saveArena(Arena arena) throws Exception {
        File arenaFile = new File(arenaFolder, arena.getName().toLowerCase().replace(" ", "_") + ".arena");
        if (!arenaFile.getParentFile().exists())
            arenaFile.getParentFile().mkdirs();
        if (!arenaFile.exists())
            arenaFile.createNewFile();
        ObjectSerializer.serialize(new SerializeArena(arena), new FileOutputStream(arenaFile));
    }

    public static File getArenaFolder() {
        return arenaFolder;
    }

}
