package de.veraty.heartbreaker.game.arena;

import lombok.Getter;
import org.bukkit.util.Vector;

public class Region {
    
    private @Getter
    double maxX, minX, minZ, maxZ, minY, maxY;

    public Region(Vector max, Vector min) {
        maxX = max.getX() > min.getX() ? max.getX() : min.getX();
        minX = max.getX() < min.getX() ? max.getX() : min.getX();
        maxY = max.getY() > min.getY() ? max.getY() : min.getY();
        minY = max.getY() < min.getY() ? max.getY() : min.getY();
        maxZ = max.getZ() > min.getZ() ? max.getZ() : min.getZ();
        minZ = max.getZ() < min.getZ() ? max.getZ() : min.getZ();
    }

    public boolean contains(Vector vector) {
        return vector.getX() <= maxX && vector.getX() > minX
                && vector.getY() <= maxY && vector.getY() > minY
                && vector.getZ() <= maxZ && vector.getZ() > minZ;
    }


}
