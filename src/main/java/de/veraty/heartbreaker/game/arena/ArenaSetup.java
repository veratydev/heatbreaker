package de.veraty.heartbreaker.game.arena;

import lombok.Getter;
import lombok.Setter;
import org.bukkit.Location;
import org.bukkit.util.Vector;

public class ArenaSetup {

    private @Getter
    @Setter
    Vector region1, region2;
    private @Getter
    @Setter
    Location spawnLocation;
    private @Getter
    @Setter
    String name;

    public ArenaSetup(String name) {
        this.name = name;
    }

    public boolean isFinish() {
        return region1 != null && region2 != null && spawnLocation != null && name != null;
    }

    public Arena finish() {
        return new Arena(name, new Region(region1, region2), spawnLocation,false);
    }

}
