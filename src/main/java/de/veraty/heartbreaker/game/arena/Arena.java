package de.veraty.heartbreaker.game.arena;

import de.veraty.heartbreaker.game.User;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.List;

public class Arena {

    private static int instances = 0;

    private final @Getter
    List<User> users;
    private final @Setter
    @Getter
    Region spawnRegion;
    private final @Setter
    @Getter
    Location spawnLocation;
    private final @Getter
    @Setter
    String name;
    private final @Getter
    @Setter
    boolean privateArena;

    private @Getter
    @Setter
    final String id;

    public Arena(String name, Region spawnRegion, Location spawnLocation, boolean privateArena) {
        this.id = "#" + (++instances);
        this.users = new ArrayList<User>();
        this.name = name;
        this.spawnLocation = spawnLocation;
        this.spawnRegion = spawnRegion;
        this.privateArena = privateArena;
    }

    public boolean containsUser(User user) {
        return users.contains(user);
    }

    public void addUser(User user) {
        user.setArena(this);
        for (User all : users) {
            all.getPlayer().showPlayer(user.getPlayer());
            user.getPlayer().showPlayer(all.getPlayer());
        }
        if (!containsUser(user))
            users.add(user);
    }

    public void removeUser(User user) {
        if (user.getPlayer().isOnline()) {
            for (User all : users) {
                all.getPlayer().hidePlayer(user.getPlayer());
                user.getPlayer().hidePlayer(all.getPlayer());
            }
        }

        if (containsUser(user))
            users.remove(user);
    }

    public void handleVisibility(Player player) {
        for (Player all : Bukkit.getOnlinePlayers()) {
            if (all != player) {
                User user = User.getInstance(all);
                if (users.contains(user)) {
                    all.showPlayer(player);
                    player.showPlayer(all);
                } else {
                    all.hidePlayer(player);
                    player.hidePlayer(all);
                }

            }
        }
    }

    public void broadcastMessage(String string) {
        for (User user : users) {
            if (user.getPlayer().isOnline())
                user.getPlayer().sendMessage(string);
        }
    }

    public Arena clone(boolean privateArena) {
        return new Arena(name, spawnRegion, spawnLocation, privateArena);
    }

}
