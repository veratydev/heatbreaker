package de.veraty.heartbreaker.game.scoreboard;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;


public class ScoreboardManager {

    private static final String MAP = " §7» §cKarte §r", HEARTS = " §7» §cHerzen §r", MODUS = " §7» §cModus §r";

    private static long lastUpdate = 0;

    public static void createScoreboard(User user, String title) {
        Player player = user.getPlayer();
        if (user.getScoreboard() == null) {
            Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();


            Objective sidebar = board.registerNewObjective("Sidebar", "dummy");
            sidebar.setDisplaySlot(DisplaySlot.SIDEBAR);
            sidebar.setDisplayName(title);

            Team hearts = board.registerNewTeam("hearts");
            Team map = board.registerNewTeam("map");
            Team modus = board.registerNewTeam("modus");

            hearts.addEntry(HEARTS);
            map.addEntry(MAP);
            modus.addEntry(MODUS);


            sidebar.getScore("§r§r§r").setScore(6);
            sidebar.getScore(HEARTS).setScore(5);
            sidebar.getScore("§r§r§l").setScore(4);
            sidebar.getScore(MAP).setScore(3);
            sidebar.getScore("§r§l§l").setScore(2);
            sidebar.getScore(MODUS).setScore(1);
            sidebar.getScore("§r").setScore(0);

            user.setScoreboard(board);
            player.setScoreboard(board);
            updateSidebar(user, title);
        }

    }

    public static void updateSidebar(User user, String title) {
        try {
            if (user.getScoreboard() == null)
                createScoreboard(user, title);

            Scoreboard board = user.getPlayer().getScoreboard();

            Team hearts = board.getTeam("hearts");
            Team map = board.getTeam("map");
            Team modus = board.getTeam("modus");

            if (hearts != null)
                hearts.setSuffix("§r §e" + user.getHearts());
            if (map != null)
                map.setSuffix("§r §e" + user.getArena().getName());
            if (modus != null)
                modus.setSuffix(user.getArena().isPrivateArena()?"§r §ePrivate FFA":"§r §ePublic FFA");

        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }


}
