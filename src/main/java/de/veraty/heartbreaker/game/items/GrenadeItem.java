package de.veraty.heartbreaker.game.items;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.beams.impl.GrenadeBeam;
import lombok.Getter;

public class GrenadeItem extends WandItem {

    private @Getter
    float gravity;

    private User user;

    public GrenadeItem(User user) {
        super(user, 80, 20, 5, 0.75f, 1);
        this.user = user;
        this.gravity = 0.5f;
    }

    public void shoot() {
        GrenadeBeam beam = new GrenadeBeam(this, user.getPlayer().getEyeLocation().getDirection(), user.getPlayer().getEyeLocation());
        beam.spawn();
    }

}
