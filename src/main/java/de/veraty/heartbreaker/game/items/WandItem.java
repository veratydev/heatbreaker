package de.veraty.heartbreaker.game.items;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.beams.impl.HeartBeam;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@AllArgsConstructor
public class WandItem {

    private @Getter
    @Setter
    User user;
    private @Getter @Setter float reach, radius, explosion, damage;
    private @Getter
    @Setter
    int speed;

    public void shoot() {
        HeartBeam beam = new HeartBeam(this, user.getPlayer().getEyeLocation().getDirection(), user.getPlayer().getEyeLocation());
        beam.spawn();
    }

}
