package de.veraty.heartbreaker.serialize;

import java.io.*;

public class ObjectSerializer {


    /**
     * Writes the object to a file
     *
     * @param serializable     Object to serialize
     * @param fileOutputStream file output stream
     */
    public static void serialize(Serializable serializable, FileOutputStream fileOutputStream) {

        if (fileOutputStream == null) return;

        ObjectOutputStream objectOutputStream = null;

        try {

            objectOutputStream = new ObjectOutputStream(fileOutputStream);
            objectOutputStream.writeObject(serializable);

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            try {
                if (objectOutputStream != null)
                    objectOutputStream.close();
                fileOutputStream.close();
            } catch (Exception exception) {
                System.exit(1); // Stop Programm (Output Stream could'nt close )
            }
        }
    }

    /**
     * Reads the object from a file
     *
     * @param fileInputStream File to read from
     * @return object
     */
    public static Object deserialize(FileInputStream fileInputStream) {

        if (fileInputStream == null) return null;

        ObjectInputStream objectInputStream = null;

        Object object = null;

        try {

            objectInputStream = new ObjectInputStream(fileInputStream);
            object = objectInputStream.readObject();

        } catch (Exception exception) {
            exception.printStackTrace();
        } finally {
            try {
                if (objectInputStream != null)
                    objectInputStream.close();
                fileInputStream.close();
            } catch (Exception exception) {
                System.exit(1); // Stop Programm (Output Stream could'nt close )
            }
        }

        return object;
    }

}
