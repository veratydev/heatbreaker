package de.veraty.heartbreaker.serialize.serializables;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.util.Vector;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class SerializeVector implements Serializable {

    private @Getter
    @Setter
    double x, y, z;


    public SerializeVector(Vector vector) {
        this.x = vector.getX();
        this.y = vector.getY();
        this.z = vector.getZ();
    }

    public Vector convert() {
        return new Vector(x, y, z);
    }

    public static List<SerializeVector> convert(List<Vector> vectors) {
        List<SerializeVector> serializeVectors = new ArrayList<SerializeVector>();
        for (Vector vector : vectors)
            serializeVectors.add(new SerializeVector(vector));
        return serializeVectors;
    }

    public static HashMap<String, SerializeVector> convert(Map<String, Vector> vectors) {
        HashMap<String, SerializeVector> serializeVectors = new HashMap<>();
        for (String key : vectors.keySet())
            serializeVectors.put(key, new SerializeVector(vectors.get(key)));
        return serializeVectors;
    }

}
