package de.veraty.heartbreaker.serialize.serializables;

import de.veraty.heartbreaker.game.arena.Arena;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;

public class SerializeArena implements Serializable {

    private @Getter
    @Setter
    SerializeLocation spawnLocation;

    private @Getter
    @Setter
    SerializeRegion spawnRegion;

    private @Getter
    @Setter
    String name;

    public SerializeArena(Arena arena) {
        this.name = arena.getName();
        this.spawnLocation = new SerializeLocation(arena.getSpawnLocation());
        this.spawnRegion = new SerializeRegion(arena.getSpawnRegion());
    }

    public Arena convert() {
        return new Arena(name, spawnRegion.convert(), spawnLocation.convert(),false);
    }

}
