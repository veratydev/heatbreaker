package de.veraty.heartbreaker.serialize.serializables;

import de.veraty.heartbreaker.game.arena.Region;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.util.Vector;

import java.io.Serializable;

@AllArgsConstructor
public class SerializeRegion implements Serializable {

    private @Getter
    @Setter
    SerializeVector max, min;

    public SerializeRegion(Region region) {
        this.max = new SerializeVector(new Vector(region.getMaxX(), region.getMaxY(), region.getMaxZ()));
        this.min = new SerializeVector(new Vector(region.getMinX(), region.getMinY(), region.getMinZ()));
    }

    public Region convert() {
        return new Region(max.convert(), min.convert());
    }

}
