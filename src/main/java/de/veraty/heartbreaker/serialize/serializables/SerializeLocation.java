package de.veraty.heartbreaker.serialize.serializables;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@AllArgsConstructor
public class SerializeLocation implements Serializable {

    private @Getter
    @Setter
    String world;
    private @Getter
    @Setter
    double x, y, z;
    private @Getter
    @Setter
    float yaw, pitch;

    public SerializeLocation(Location location) {
        this.world = location.getWorld().getName();
        this.x = location.getX();
        this.y = location.getY();
        this.z = location.getZ();
        this.yaw = location.getYaw();
        this.pitch = location.getPitch();
    }

    public Location convert() {
        World world = Bukkit.getWorld(this.world);
        if (world == null) world = Bukkit.getWorlds().get(0);
        return new Location(world, x, y, z, yaw, pitch);
    }

    public static List<SerializeLocation> convert(List<Location> locations) {
        List<SerializeLocation> serializeLocations = new ArrayList<SerializeLocation>();
        for (Location location : locations)
            serializeLocations.add(new SerializeLocation(location));
        return serializeLocations;
    }

    public static HashMap<String, SerializeLocation> convert(Map<String, Location> locations) {

        HashMap<String, SerializeLocation> serializeLocations = new HashMap<>();
        for (String key : locations.keySet())
            serializeLocations.put(key, new SerializeLocation(locations.get(key)));
        return serializeLocations;
    }

}
