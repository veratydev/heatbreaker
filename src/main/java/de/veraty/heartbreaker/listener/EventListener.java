package de.veraty.heartbreaker.listener;

import de.veraty.heartbreaker.HeartBreaker;
import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

public abstract class EventListener implements Listener {

    public void register() {
        Bukkit.getPluginManager().registerEvents(this, HeartBreaker.getInstance());
    }

}
