package de.veraty.heartbreaker.listener.inventory;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.scheduler.BukkitRunnable;

public class InventoryCloseListener extends EventListener {

    @EventHandler
    public void onCalled(InventoryCloseEvent event) {
        new BukkitRunnable() {
            @Override
            public void run() {
                if (event.getInventory().getTitle() != null) {
                    for (InventoryOpener opener : HeartBreaker.getInstance().getInventoryOpeners()) {
                        if (opener.matches(event.getInventory()))
                            opener.onClose(User.getInstance((Player) event.getPlayer()));
                    }
                }
            }
        }.runTaskLater(HeartBreaker.getInstance(), 5l);


    }

}

