package de.veraty.heartbreaker.listener.inventory;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.helper.InventoryOpener;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.inventory.InventoryClickEvent;

public class InventoryClickListener extends EventListener {

    @EventHandler
    public void onCalled(InventoryClickEvent event) {
        event.setCancelled(true);
        if (event.getCurrentItem() != null && event.getCurrentItem().hasItemMeta() && event.getCurrentItem().getItemMeta().hasDisplayName()) {
            if (event.getClickedInventory().getTitle() != null) {
                for (InventoryOpener opener : HeartBreaker.getInstance().getInventoryOpeners()) {
                    if (opener.matches(event.getClickedInventory()))
                        opener.onClick(event);
                }
            }
        }
    }

}
