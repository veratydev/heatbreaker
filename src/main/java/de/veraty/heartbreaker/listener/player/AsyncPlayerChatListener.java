package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.AsyncPlayerChatEvent;

public class AsyncPlayerChatListener extends EventListener {

    @EventHandler
    public void onCalled(AsyncPlayerChatEvent event) {
        event.setCancelled(true);
        User user = User.getInstance(event.getPlayer());
        if (user.getArena() != null) {
            user.getArena().broadcastMessage("§3" + user.getPlayer().getDisplayName() + " §8» §7" + event.getMessage().replace("<3", "§4§l❤§r§7"));
        }
    }

}


