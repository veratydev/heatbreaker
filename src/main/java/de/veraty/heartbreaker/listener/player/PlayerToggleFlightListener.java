package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.game.Ability;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerToggleFlightEvent;

public class PlayerToggleFlightListener extends EventListener {

    @EventHandler
    public void onCalled(PlayerToggleFlightEvent event) {
        event.setCancelled(true);
        User user = User.getInstance(event.getPlayer());
        if (user.getAbility() == Ability.DOUBLE_JUMP && !user.hasJumpCooldown()) {
            user.setJumpCooldown(System.currentTimeMillis());
            user.playSound(Sound.ENDERDRAGON_WINGS);
            user.getPlayer().setVelocity(user.getPlayer().getLocation().getDirection().multiply(0.9).setY(1));
        }
    }

}
