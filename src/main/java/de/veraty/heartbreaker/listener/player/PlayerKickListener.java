package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerKickEvent;

public class PlayerKickListener extends EventListener {

    @EventHandler
    public void onCalled(PlayerKickEvent event) {
        event.setLeaveMessage("");
        User.getInstance(event.getPlayer()).onLeave();
    }

}

