package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener extends EventListener {

    @EventHandler
    public void onCalled(PlayerJoinEvent event) {
        event.setJoinMessage("");
        User user = User.getInstance(event.getPlayer());
        if (user.getArena() != null) {
            user.onSpawn(user.getArena());
        } else {
            if (HeartBreaker.getInstance().getArenaManager().getLoadedArenas().size() > 0) {
                user.onSpawn(HeartBreaker.getInstance().getArenaManager().getLoadedArenas().get(0));
            }
        }
        de.veraty.heartbreaker.game.scoreboard.ScoreboardManager.updateSidebar(user, HeartBreaker.getScoreboard());
    }

}
