package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.event.EventHandler;
import org.bukkit.event.player.PlayerQuitEvent;

public class PlayerQuiListener extends EventListener {

    @EventHandler
    public void onCalled(PlayerQuitEvent event) {
        event.setQuitMessage("");
        User.getInstance(event.getPlayer()).onLeave();
    }

}
