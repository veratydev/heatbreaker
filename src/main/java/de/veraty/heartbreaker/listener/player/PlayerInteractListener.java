package de.veraty.heartbreaker.listener.player;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.Ability;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.helper.impl.AbilityInventoryOpener;
import de.veraty.heartbreaker.helper.impl.ArenasInventoryOpener;
import de.veraty.heartbreaker.helper.impl.shop.ShopInventoryOpener;
import de.veraty.heartbreaker.listener.EventListener;
import org.bukkit.Sound;
import org.bukkit.event.EventHandler;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

public class PlayerInteractListener extends EventListener {

    @EventHandler
    public void onCalled(PlayerInteractEvent event) {
        event.setCancelled(true);
        if (event.getItem() != null && event.getAction() != Action.PHYSICAL) {
            if (event.getItem().hasItemMeta() && event.getItem().getItemMeta().hasDisplayName()) {
                String name = event.getItem().getItemMeta().getDisplayName();
                User user = User.getInstance(event.getPlayer());
                if (name.contains("Setup")) {
                    if (user.getSetup() != null) {
                        if (event.getAction() == Action.RIGHT_CLICK_BLOCK) {
                            user.getSetup().setRegion1(event.getClickedBlock().getLocation().toVector());
                            user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die 2. Position der Region gesetzt");
                        } else if (event.getAction() == Action.LEFT_CLICK_BLOCK) {
                            user.getSetup().setRegion2(event.getClickedBlock().getLocation().toVector());
                            user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die 1. Position der Region gesetzt");
                        } else if (event.getAction() == Action.LEFT_CLICK_AIR) {
                            user.getSetup().setSpawnLocation(event.getPlayer().getLocation());
                            user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die Spawnposition gesetzt");
                        }
                    }
                } else if (name.contains("Heartbreaker")) {
                    if (event.getAction() == Action.LEFT_CLICK_AIR) {
                        if (user.getArena() != null) {
                            if (HeartBreaker.getInstance().getGame().isSpawnArea(event.getPlayer().getLocation().toVector())) {
                                user.playSound(Sound.ITEM_BREAK);
                            } else {
                                if (!user.hasBeamCooldown()) {
                                    user.getWandItem().shoot();
                                    user.playSound(Sound.ITEM_PICKUP);
                                    user.setBeamCooldown(System.currentTimeMillis());

                                } else {
                                    user.playSound(Sound.ITEM_BREAK);
                                }
                            }
                        }
                    }
                } else if (name.contains("Granate")) {
                    if (event.getAction() == Action.RIGHT_CLICK_AIR) {
                        if (user.getArena() != null) {
                            if (HeartBreaker.getInstance().getGame().isSpawnArea(event.getPlayer().getLocation().toVector())) {
                                user.playSound(Sound.ITEM_BREAK);
                            } else {
                                if (!user.hasGrenadeCooldown()) {
                                    user.getGrenadeItem().shoot();
                                    user.playSound(Sound.IRONGOLEM_THROW);
                                    user.setGrenadeCooldown(System.currentTimeMillis());
                                    if (event.getItem().getAmount() > 1)
                                        event.getItem().setAmount(event.getItem().getAmount() - 1);
                                    else
                                        user.getPlayer().getInventory().remove(event.getItem());
                                } else {
                                    user.playSound(Sound.ITEM_BREAK);
                                }
                            }
                        }
                    }
                } else if (name.contains("Shop")) {
                    if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                        event.getPlayer().openInventory(ShopInventoryOpener.getInstance().getInventory());
                        user.playSound(Sound.CHEST_OPEN);
                    }
                } else if (name.contains("Arenen")) {
                    if (!HeartBreaker.getInstance().getGame().isSpawnArea(event.getPlayer().getLocation().toVector())) {
                        user.playSound(Sound.ITEM_BREAK);
                    } else {
                        if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                            event.getPlayer().openInventory(ArenasInventoryOpener.getInstance().getInventory());
                            user.playSound(Sound.CHEST_OPEN);
                        }
                    }
                } else {

                    for (Ability ability : Ability.values()) {
                        if (ability.getIcon().getItemMeta().getDisplayName().equals(name)) {
                            if (HeartBreaker.getInstance().getGame().isSpawnArea(user.getPlayer().getLocation().toVector()) || ability == Ability.NONE) {
                                if (event.getAction() == Action.RIGHT_CLICK_BLOCK || event.getAction() == Action.RIGHT_CLICK_AIR) {
                                    event.getPlayer().openInventory(AbilityInventoryOpener.getInstance().getInventory());
                                    user.playSound(Sound.CHEST_OPEN);
                                }
                            } else {
                                user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du must im Spawn sein um deine Fähigkeit auszuwählen");
                                user.playSound(Sound.ITEM_BREAK);
                            }
                        }
                    }

                }
            }
        }
    }

}
