package de.veraty.heartbreaker.commands;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.Game;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.arena.Arena;
import de.veraty.heartbreaker.helper.impl.ArenasInventoryOpener;
import net.md_5.bungee.api.chat.ClickEvent;
import net.md_5.bungee.api.chat.ComponentBuilder;
import net.md_5.bungee.api.chat.HoverEvent;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class ArenaCommand implements CommandExecutor {

    private final String[] help = new String[]{
            HeartBreaker.getPrefix() + "Arena Commands:",
            "§e /arena create <map>",
            "§e /arena invite <Player>",
            "§e /arena accept <Player>",
            "§e /arena leave",
            "§e /arena list"
    };

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            if (args.length == 0) {
                sender.sendMessage(help);
            } else {
                User user = User.getInstance((Player) sender);
                if (args[0].equalsIgnoreCase("create")) {
                    if (user.getArena().isPrivateArena()) {
                        user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du bist bereits in einer privaten Arena");
                    } else {
                        Arena someArena = null;
                        Arena arenaRequested = null;
                        String mapRequest = null;
                        if (args.length > 1)
                            mapRequest = args[1];
                        for (Arena arena : HeartBreaker.getInstance().getArenaManager().getLoadedArenas()) {
                            if (!arena.isPrivateArena()) {
                                someArena = arena;
                                if (mapRequest != null && arena.getName().equalsIgnoreCase(mapRequest)) {
                                    arenaRequested = arena;
                                }
                            }
                        }
                        if (arenaRequested != null) someArena = arenaRequested;
                        if (mapRequest != null && arenaRequested == null) {
                            sender.sendMessage(HeartBreaker.getPrefix() + "Es wurde keine Arena mit dieser Karte gefunden... ");
                            return true;
                        }
                        if (someArena != null) {
                            Arena privateArena = someArena.clone(true);
                            user.onSpawn(privateArena);
                            sender.sendMessage(HeartBreaker.getPrefix() + "Eine private Arena wurde erstellt");
                        } else {
                            sender.sendMessage(HeartBreaker.getPrefix() + "Es wurde keine Arena gefunden");
                            return true;
                        }

                    }
                } else if (args[0].equalsIgnoreCase("invite")) {
                    if (user.getArena() != null && user.getArena().isPrivateArena()) {
                        if (args.length == 1) {
                            sender.sendMessage(HeartBreaker.getPrefix() + "§e/arena invite <Player>");
                        } else {
                            Player player = Bukkit.getPlayer(args[1]);
                            if (player != null) {
                                User target = User.getInstance(player);
                                if (target.getArenaInvitations().contains(user.getPlayer().getUniqueId())) {
                                    sender.sendMessage(HeartBreaker.getPrefix() + "Du hast diesen Spieler bereits eingeladen");
                                } else {
                                    target.getArenaInvitations().add(user.getPlayer().getUniqueId());
                                    target.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du wurdest von " + ((Player) sender).getDisplayName() + " §7in eine Arena eingeladen");
                                    TextComponent component = new TextComponent(HeartBreaker.getPrefix());
                                    TextComponent extra = new TextComponent("§e/arena accept " + ((Player) sender).getDisplayName());
                                    extra.setClickEvent(new ClickEvent(ClickEvent.Action.RUN_COMMAND, "/arena accept " + ((Player) sender).getDisplayName()));
                                    extra.setHoverEvent(new HoverEvent(HoverEvent.Action.SHOW_TEXT, new ComponentBuilder("§7Trete der Arena bei").create()));
                                    component.addExtra(extra);
                                    target.getPlayer().spigot().sendMessage(component);
                                    sender.sendMessage(HeartBreaker.getPrefix() + "Du hast " + target.getPlayer().getDisplayName() + "§7 eingeladen");
                                }
                            } else {
                                sender.sendMessage(HeartBreaker.getPrefix() + "Dieser Spieler ist nicht Online");
                            }
                        }
                    } else {
                        sender.sendMessage(HeartBreaker.getPrefix() + "Du musst in einer privaten Arena sein");
                    }
                } else if (args[0].equalsIgnoreCase("accept")) {
                    if (args.length == 1) {
                        sender.sendMessage(HeartBreaker.getPrefix() + "§e/arena accept <Player>");
                    } else {
                        Player player = Bukkit.getPlayer(args[1]);
                        if (player != null) {
                            User target = User.getInstance(player);
                            if (!user.getArenaInvitations().contains(player.getUniqueId())) {
                                sender.sendMessage(HeartBreaker.getPrefix() + "Dieser Spieler hat dich nicht eingeladen");
                            } else {
                                if (target.getArena() != null) {
                                    user.getArenaInvitations().remove(target.getPlayer().getUniqueId());
                                    user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die Einladung von " + ((Player) sender).getDisplayName() + " §7angenommen");
                                    target.getPlayer().sendMessage(HeartBreaker.getPrefix() + ((Player) sender).getDisplayName() + " §7hat deine Einladung angenommen");
                                    user.onSpawn(target.getArena());
                                } else {
                                    sender.sendMessage(HeartBreaker.getPrefix() + "Dieser Spieler ist in keiner Arena");
                                }
                            }
                        } else {
                            sender.sendMessage(HeartBreaker.getPrefix() + "Dieser Spieler ist nicht Online");
                        }
                    }
                } else if (args[0].equalsIgnoreCase("leave")) {
                    if (user.getArena().isPrivateArena()) {
                        user.onSpawn(Game.findPublicArena());
                        user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du hast die private Arena verlassen");
                    } else {
                        user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du bist in keiner privaten Arena");
                    }
                } else if (args[0].equalsIgnoreCase("list")) {
                    user.getPlayer().openInventory(ArenasInventoryOpener.getInstance().getInventory());
                }
            }
        } else {
            sender.sendMessage("§cDu musst eine Spieler sein");
        }
        return true;
    }
}
