package de.veraty.heartbreaker.commands;

import de.veraty.heartbreaker.HeartBreaker;
import de.veraty.heartbreaker.game.User;
import de.veraty.heartbreaker.game.arena.Arena;
import de.veraty.heartbreaker.game.arena.ArenaSetup;
import de.veraty.heartbreaker.utils.ItemBuilder;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class HeartBreakerCommand implements CommandExecutor {

    public static final String PERM_ROOT = "heartbreaker", PERM_HELP = PERM_ROOT + ".help", PERM_SETUP = PERM_ROOT + ".setup";

    private String[] help = new String[]{
            HeartBreaker.getPrefix() + "Eine Liste aller Befehle",
            "§7» §e/heartbreaker setup <id>",
            "§7» §e/heartbreaker setup finish"
    };

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (args.length < 2) {
            if (sender.hasPermission(PERM_HELP))
                sender.sendMessage(help);
            else sender.sendMessage(HeartBreaker.getNoPermission());
        } else {
            if (sender.hasPermission(PERM_SETUP)) {
                if (sender instanceof Player) {
                    User user = User.getInstance(((Player) sender));
                    if (args[1].equalsIgnoreCase("finish")) {
                        if (user.getSetup() != null && user.getSetup().isFinish()) {
                            Arena arena = user.getSetup().finish();
                            try {
                                HeartBreaker.getInstance().getArenaManager().saveArena(arena);
                                user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Die Arena §e" + arena.getName() + "§7 wurde gespeichert");
                                user.setSetup(null);
                            } catch (Exception e) {
                                e.printStackTrace();
                                user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "§cDie Arena konnte nicht gespeichert werden");
                            }
                        } else {
                            user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "§cEs wurde kein fertiges Setup gefunden");
                        }
                    } else {
                        user.setSetup(new ArenaSetup(args[1]));
                        user.getPlayer().sendMessage(HeartBreaker.getPrefix() + "Du kannst nun die Arena §e" + args[1] + "§7 einrichten");
                        user.getPlayer().getInventory().clear();
                        user.getPlayer().getInventory().setItem(0, ItemBuilder.SETUP);
                    }
                } else {
                    sender.sendMessage(HeartBreaker.getPrefix() + "Du musst ein Spieler sein!");
                }
            } else {
                sender.sendMessage(HeartBreaker.getNoPermission());
            }
        }
        return true;
    }
}
